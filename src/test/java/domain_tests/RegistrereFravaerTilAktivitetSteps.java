package domain_tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import domain.AbsenceTime;
import domain.Application;
import domain.Employee;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class RegistrereFravaerTilAktivitetSteps {
 
	private AbsenceTime aT;
	
	private LocalDate startDate;
	private LocalDate endDate;
	private String reason;
	private Employee e = Application.employees.get(0);

	@Given("a user creates a new absence activity with a start-, end-date and reason for absence")
	public void aUserCreatesANewAbsenceActivityWithAStartEndDateAndReasonForAbsence() {
	  
	  startDate = LocalDate.now();
	  endDate = LocalDate.parse("2022-12-12");
	  reason = "Sick";
			
	  aT = new AbsenceTime(startDate, endDate, reason);
	  assertEquals(startDate,aT.getStartDate());
	  assertEquals(endDate,aT.getEndDate());
	  assertEquals(reason,aT.getReason());
	   
	}
	
	@Then("the absence is registered for the user")
	public void theAbsenceIsRegisteredForTheUser() {
		e.setAbsence(aT);
		assertEquals(1,e.getAbsence().size());
		assertEquals(reason,aT.getReason());
	}

}

