package domain_tests;

import static org.junit.Assert.assertEquals;

import domain.Activity;
import domain.ProjectManager;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TilfoejForventetTidSteps {
	
	private Activity activity = new Activity("test");
	 
	// Scenario1&2
	@When("the project manager changes the estimated time of the activity to {double} hours")
	public void theProjectManagerChangesTheEstimatedTimeOfTheActivityToHours(Double time) {
		activity.setBudgetTime(time);
	}

	@Then("the estimated time of the activity is {double} hours")
	public void theEstimatedTimeOfTheActivityIsHours(Double time) {
		assertEquals(time, (Double) activity.getBudgetTime());
	}
	
}

