package domain_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import domain.Application;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegistrereTimerTilAktivitetSteps {

	private Project p;
	private Employee e1;
	private Employee e = Application.employees.get(0);
	
	public RegistrereTimerTilAktivitetSteps(Project p) {
		this.p = p;	
 	}
	
	@When("the employee has {string} hours of time on activity")
	public void theEmployeeHasHoursOfTimeOnActivity(String hours) {
		Double dhours = Double.parseDouble(hours);
		p.getActivities().get(0).setWorkTime(this.e,dhours);
	}

	@Then("{string}  hours are registered for the activity")
	public void hoursAreRegisteredForTheActivity(String string) {
		assertEquals("3.0",
				p.getActivities().get(0).getWorkTime().get(e).toString());
	}
 
	@Given("an activity exists in a project")
	public void anActivityExistsInAProject() {
		assertTrue(p.getActivities().size() >= 1);
	}
	 
	@Given("another users then adds {string} hours of time worked on the activity")
	public void anotherUsersThenAddsHoursOfTimeWorkedOnTheActivity(String time) {
		Double dtime = Double.parseDouble(time);
		 e1 = new Employee("bob");
		Application.employees.add(e1);

		p.getActivities().get(0).setWorkTime(e1,dtime);
	}

	@Then("the time both employees have worked is registered on the activity")
	public void theTimeBothEmployeesHaveWorkedIsRegisteredOnTheActivity() {
		assertEquals("3.0",p.getActivities().get(0).getWorkTime().get(e).toString());
		assertEquals("5.0",p.getActivities().get(0).getWorkTime().get(e1).toString());
	}
	
}

