package domain_tests;

import static org.junit.Assert.assertEquals;

import domain.Activity;
import domain.Application;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TilfoejMedarbejderTilAktivitetSteps {
	
	private Project p;
	private Application app;
	private Activity act;
	private Employee e = Application.employees.get(0);

	public TilfoejMedarbejderTilAktivitetSteps(Project p,Application app) {
		this.p = p;
		this.app = app;
 	}
	 
	@Given("the project has an activity")
	public void theProjectHasAnActivity() {
		act = new Activity("activit 1");
		p.setActivities(act);
		assertEquals(1,p.getActivities().size());
	}
		
	@Given("the employee is not assigned the activity.")
	public void theEmployeeIsNotAssignedTheActivity() {
		int i = Application.employees.size();
		assertEquals(i+1,e.getActivities().size());
	}
	
	@When("the employee is assigned to the activity.")
	public void theEmployeeIsAssignedToTheActivity() {
		e.setActivity(p.getActivities().get(0));
	}
	
	@Then("The employee is assigned the activity.")
	public void theEmployeeIsAssignedTheActivity() {
		int j = e.getActivities().size();
		assertEquals(j,e.getActivities().size());
	}
	
	
	
}

