package domain_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.Locale;

import domain.Activity;
import domain.Project;
import domain.ProjectManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TilfoejAktivitetSteps {
	
	private Activity activity;
	private String activityName = "some activity name"; 
	private Project p;
	ProjectManager pa = new ProjectManager("manager");
	
	public TilfoejAktivitetSteps(Project p) {
		this.p = p;
	}
	 

	@When("the project manager adds an activity with start time {string} and deadline {string}")
	public void theProjectManagerAddsAnActivityWithStartTimeAndDeadline(String start, String end) {
		LocalDate startdate = LocalDate.parse(start); 
		LocalDate enddate = LocalDate.parse(end); 

		WeekFields sweeks = WeekFields.of(Locale.getDefault()); 
		int intSDate = startdate.get(sweeks.weekOfWeekBasedYear());

		WeekFields eweeks = WeekFields.of(Locale.getDefault()); 
		int intEDate = enddate.get(eweeks.weekOfWeekBasedYear());
		
		activity = new Activity(activityName,startdate,enddate);
		assertTrue(pa.projectManagerExist());
		assertEquals(intSDate,activity.getStartDateAsWeek());
		assertEquals(intEDate,activity.getEndDateAsWeek());
	}

	@Then("the activity is added to the project")
	public void theActivityIsAddedToTheProject() throws Exception {
		p.setActivities(activity);
		assertEquals(p.getActivities().get(0),activity);
	}
	
 	@Given("there exists an activity")
	public void thereExistsAnActivity() throws Exception {
		activity = new Activity(activityName);
		p.setActivities(activity);
		assertEquals(1,p.getActivities().size());
	}

	@When("the project manager changes the budgeted time of the activity to {double} hours")
	public void theProjectManagerChangesTheBudgetedTimeOfTheActivityToHours(double time) throws Exception {
		activity.setBudgetTime(time);
	}

	@Then("the budgeted time of the activity is {double} hours")
	public void theBudgetedTimeOfTheActivityIsHours(double time) throws Exception {
		assertTrue(time == activity.getBudgetTime());
	}
	
}

