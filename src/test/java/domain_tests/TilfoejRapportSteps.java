package domain_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Map;

import domain.AbsenceTime;
import domain.Activity;
import domain.Employee;
import domain.Project;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class TilfoejRapportSteps {
	
	private Project p;
	private Project report;
	private Employee e1;
	
	public TilfoejRapportSteps(TilfoejProjektSteps e) {
		this.p = e.project;
		report = p.generateReport();
	}
	
	/**
	 * background for rapport
	 */


	@Given("the project has 2 activites")
	public void theProjetHasActivites() {
		p.setActivities(new Activity("activity 1"));
		p.setActivities(new Activity("activity 2"));
		assertEquals(2,p.getActivities().size());
	}
	
	@Given("both activities have {double} hours as the budgetted time")
	public void bothActivitiesHaveHoursAsTheBudgettedTime(Double int1) {
		p.getActivities().get(0).setBudgetTime(int1);
		p.getActivities().get(1).setBudgetTime(int1);
	}

	@Given("activity 1 has one employee assigned that has worked {double} hours")
	public void activityHasOneEmployeeAssignedThatHasWorkedHours(Double hours) {
	   	e1 = new Employee("employee 1");
	    p.getActivities().get(0).setWorkTime(e1,hours);
	    assertEquals(hours, p.getActivities().get(0).getWorkTime().get(e1));
	}
	
	@Given("activity 2 has 2 employees assigned that has worked {double} hours each")
	public void activityHasEmployeesAssignedThatHasWorkedHoursEach(Double hours) {
	   Employee e2 = new Employee("employee 2");
	    p.getActivities().get(1).setWorkTime(e2, hours);
	    p.getActivities().get(1).setWorkTime(e1, hours);

	    assertEquals(hours, p.getActivities().get(1).getWorkTime().get(e2));
	    assertEquals(hours, p.getActivities().get(1).getWorkTime().get(e1));
	}
	
	@Given("one of the employees have had absence between {string} to {string} with the reason {string}")
	public void oneOfTheEmployeesHaveHadAbsenceBetweenToWithTheReason(String start, String end, String reason) {
		e1.setAbsence(
				new AbsenceTime(
					LocalDate.parse(start),LocalDate.parse(end),reason));
		assertEquals(LocalDate.parse(start),e1.getAbsence().get(0).getStartDate());
	}
	
	/**
	 * actual step definition scenarios
	 */
		
	 
	
	@Given("the report contains all the projects activities")
	public void theReportContainsAllTheProjectsActivities() {
		assertEquals(p.getActivities(),report.getActivities());
	}
	
	@Given("employees time and name that have spend time on each activity")
	public void employeesTimeAndNameThatHaveSpendTimeOnEachActivity() {
		// validate hashmap values are based on the background steps
		p.getActivities().forEach(activity -> 
			activity.getWorkTime().forEach((k,j) -> {
					assertTrue(k instanceof Employee);
					assertTrue(j == 6.0 | j == 5.5);
				}
			)
		);
	}
	
	@Given("absence of employees that have worked on the project")
	public void absenceOfEmployeesThatHaveWorkedOnTheProject() {
	    // validate absence is part of one of the employees object
		assertTrue(p.getEmployeesOnProject().get(0).getAbsence().size() >= 1);
	}
	
	@Given("total spent time on each activity")
	public void totalSpentTimeOnEachActivity() {
		// assert values are within project descriptions defined range
		p.getActivities().forEach(a -> 
			assertTrue(a.getTotalSpendTimeOnActivity() >= 5.5 && 
				a.getTotalSpendTimeOnActivity() <= 12));
	}
	
	@Given("total spent time on the project")
	public void totalSpentTimeOnTheProject() {
		
		assertTrue(p.getTotalTimeSpendOnProject() == 17.5);
	}
	
	@Given("budgetted time")
	public void budgettedTime() {
		assertTrue(p.getTotalBudgetTimeOnProject() == 20.00);
	}
	
	@Then("a report is returned to the user")
	public void aReportIsReturnedToTheUser() {
	    assertTrue(p.generateReport() instanceof Project);
	}
	


}

