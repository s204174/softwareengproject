package domain_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.mockito.Matchers;

import domain.Application;
import domain.Employee;
import domain.Project;
import domain.ProjectManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TilfoejProjektSteps {
	
	Project project;
	
	
	@Given("User is registered under softwarehus A\\/S")
	public void userIsRegisteredUnderSoftwarehusAS() {
			int count = Application.employees.size();
			Application.employees.add(new Employee("Lukas"));
			assertEquals(count+1,Application.employees.size());
	}
	
	@When("a user creates a new project called {string} in year twenty-twenty-one")
	public void aUserCreatesANewProjectCalledInYearTwentyTwentyOne(String string) {
		project = new Project();
	}
	
	@Then("a new project is created with an id number \\(year_postfix-unique_id)")
	public void aNewProjectIsCreatedWithAnIdNumberYearPostfixUniqueId() {
 		SimpleDateFormat format = new SimpleDateFormat("yyyy");
 		String formattedDate = format.format(new Date());
 		String year = Integer.toString(Integer.parseInt(formattedDate)).substring(2);
 		
 		assertFalse(project.getId().isEmpty());
 		assertTrue(year.equals(project.getId().substring(0,2)));

	}
	
	@Given("there exists a project")
	public void thereExistsAProject() {
		project = new Project();
		Application.projects.add(project);
	}
	
	@Given("there is a project with title {string}")
	public void thereIsAProjectWithTitle(String string) {
		project.setProjectName(string);
		assertEquals(string,project.getProjectName());
	}
	
	@Given("project {string} has no project manager")
	public void projectHasNoProjectManager(String string) {
		assertEquals(null,project.getProjectManager());
	}
	
	@When("project manager is assigned to project")
	public void projectManagerIsAssignedToProject() {
		ProjectManager pm = new ProjectManager("hans");
		project.setProjectManager(pm);
	}
	
	@Then("project {string} has user as manager")
	public void projectHasUserAsManager(String string) {
		assertFalse(project.getProjectManager().equals(null));
	}
	
	@Given("the project has a project manager")
	public void theProjectHasAProjectManager() {
		projectManagerIsAssignedToProject();
		assertNotEquals(null,project.getProjectManager());
	}
	
	@When("an employee is assigned as the new project manager")
	public void amEmployeeIsAssignedAsTheNewProjectManager() {
		project.setProjectManager(new ProjectManager("lukas"));
		assertEquals("lukas",project.getProjectManager().getName());
	}
	

	
	@Then("the name of the project is {string}")
	public void theNameOfTheProjectIs(String string) {
		assertEquals(string,project.getProjectName());
	}
	
	
	@When("the user changes the name to {string}")
	public void theUserChangesTheNameTo(String string) {
		project.setProjectName(string);
	}


	
}

