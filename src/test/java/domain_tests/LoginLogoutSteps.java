package domain_tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import domain.Application;
import domain.Employee;
import domain.UserNotFoundException;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class LoginLogoutSteps {
	
	private Employee e;
	private Application app;
	private ErrorMessageHolder errorMessage;
	
	public LoginLogoutSteps(Application app, ErrorMessageHolder errorMessage) {
		this.app = app;
		this.errorMessage = errorMessage;
	}
	


	@Given("an account with the name {string} and password {string} exists")
	public void anAccountWithTheNameAndPasswordExists(String name, String psw) {

		e = new Employee(name);
	    e.setPassword(psw);

	    assertEquals(name, e.getName());
	    assertEquals(psw, e.getPassword());
	    assertTrue(!e.getEmployeeId().equals(""));

	    // add employee to the in-memory db
		Application.employees.add(e);
	}
	 

	@Given("the user logs in with the employee id and password {string}")
	public void theUserLogsInWithTheEmployeeIdAndPassword(String psw) {
		String eId = e.getEmployeeId();
		try {
			app.login(eId, psw);
		} catch (UserNotFoundException e) {
			errorMessage.setErrorMessage(e.getMessage());
		}
	}

	@Given("the user logs in with employee id {string} and password {string}")
	public void theUserLogsInWithEmployeeIdAndPassword(String eId, String psw) {
		try {
			app.login(eId, psw);
		} catch (UserNotFoundException e) {
			errorMessage.setErrorMessage(e.getMessage());
		}
	}


	@Then("the user is loggedin")
	public void theUserIsLoggedin() {
		assertTrue(Application.employees.get(0).getIsUserLoggedin());
	}
	

	/**
	 * error message
	 * @param errorMessage
	 * @throws Exception
	 */
	@Then("the error message {string} is given")
	public void theErrorMessageIsGiven(String errorMessage) throws Exception {
		assertEquals(errorMessage, this.errorMessage.getErrorMessage());
	}
	
	
	/**
	 * logout features
	 */
	
	@Given("the user is logged in")
	public void theUserIsLoggedIn() {
		Application.employees.get(0).setIsUserLoggedin(true);
		assertTrue(Application.employees.get(0).getIsUserLoggedin());
	}
	 
	
	@Given("the user tries to log out")
	public void theUserTriesToLogOut() {
		Application.employees.get(0).setIsUserLoggedin(false);
		app.logout(e.getEmployeeId());
	}
	
	@Then("the user is logged out")
	public void theUserIsLoggedOut() {
		assertFalse(Application.employees.get(0).getIsUserLoggedin());
	}



}

