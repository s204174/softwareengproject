package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import domain.AbsenceTime;
import domain.Activity;
import domain.Application;
import domain.Employee;
import domain.Project;
import domain.ProjectManager;
import domain.UserNotFoundException;

public final class FrontController {
	   
	private static final FrontController instance  = new FrontController();
    
    private FrontController(){}
    
    public static FrontController getInstance(){
        return instance;
    }
    
    private static final Application app = new Application();
 
    /**
     * methods for logging in and logging out users
     */
    
    /**
     * 
     * @param employeeId
     * @param password
     * @return
     * @throws UserNotFoundException
     */
    public boolean login(String employeeId, String password) throws UserNotFoundException {
    	return app.login(employeeId, password);
    }

    /**
     * 
     * @param employeeId
     */
	public void logout(String employeeId) {
		app.logout(employeeId);		
	}
	
	
	
	/**
	 * employee methods
	 */
    
    
    /**
     * create a new account 
     * 
     * @param accountType
     * @param name
     * @param password
     * @return true if account is successfully created 	| false
     * @throws Exception 
     */
	
    public Employee createUser(String accountType, String name, String password) throws Exception {
    	if (accountType.equals("1")) {
    		// new employee
    		Employee e = new Employee(name);
    	    e.setPassword(password);
    	    Application.employees.add(e);
    	    
    	    // console employee id for testing purposes
    	    view.controller.LoginController.dummyLoginString = "You can login with the dummy account; employee id: " 
    	    													+ e.getEmployeeId() + " and password: " + e.getPassword();
    	    
    	    return e;
    	    
    	} else if (accountType.equals("2")) {
    		// new project manager
    		ProjectManager e = new ProjectManager(name);
    	    e.setPassword(password);
    	    Application.employees.add(e);
    	    
    	    // console employee id for testing purposes
    	    System.out.println("Employee id: " + e.getEmployeeId() + "\nPassword: " + e.getPassword());
    	    
    	    return e;
    	}
    	throw new Exception("Coldn't create account");
    }
    
    
    /**
     * get employee by employee id 
     * 
     * @param id
     * @return
     * @throws Exception
     */


	public Employee getEmployeeById(String id) throws Exception {
		for (int i = 0; i < Application.employees.size(); i++) {
			if (Application.employees.get(i).getEmployeeId().equals(id)) {
				return Application.employees.get(i);
			}
		}
		throw new Exception("Employee not found");
	}

	
	
    
    /**
     * project methods
     */
    
    /**
     * create a new project
     * @param string
     * @return project
     */

	public Project createProject(String projectName) {
		Project project = new Project();
		if (!(projectName.equals("") && projectName == null)) {
			project.setProjectName(projectName);
		}		
		Application.projects.add(project);
		return project;
	}

	/**
	 * update a project name
	 * @param project
	 * @param projectName
	 */
	public void updateProjectName(Project project, String projectName) {
		project.setProjectName(projectName);
	}
 
	
	/**
	 * update a projects project manager
	 * @param project
	 * @param newManager
	 */
	public void updateProjectManager(Project project, Employee newManager) {
		
		// project manager already exist on project
		if (project.getProjectManager()!=null) {
			System.out.println("kl");
			 
			// if project has a manager unassign him
			if(project.getProjectManager()!=null) {
				List<Project> p = project.getProjectManager().getProjects();
				List<Project> newProjectList = new ArrayList<Project>();
				for (int i = 0; i < p.size(); i++) {
					if (!(p.get(i).equals(project))) {
						newProjectList.add(project);
					}
				}
				project.getProjectManager().setProjects(newProjectList);
			}
			
		}
		
		// if employee is not a project manager make him one
		// and cast to project manager
		ProjectManager manager;
		if (!newManager.projectManagerExist()) {
			manager = new ProjectManager(newManager);
		} else {
			manager = (ProjectManager) newManager;
		}
		
		// add to list of managers projects
		manager.setProjects(project);
		
		// set project
		project.setProjectManager(manager);
	}
	
	/**
	 * get a project by project id 
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Project getProjectById(String id) throws Exception {
		for (int i = 0; i < Application.projects.size(); i++) {
			if (Application.projects.get(i).getId().equals(id)) {
				return Application.projects.get(i);
			}
		}
		throw new Exception("Project not found");
	}
	
	
	
	
	
	
	/**
	 * create absence
	 * 
	 * @param employeeId
	 * @param from
	 * @param to
	 * @param reason
	 */

	public void setAbsence(String employeeId, LocalDate from, LocalDate to, String reason) {
		Employee e;
		try {
			e = this.getEmployeeById(employeeId);
			e.setAbsence(new AbsenceTime(from,to,reason));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * adds time or employee and time to an activity that's part of a project
	 * @param project
	 * @param activityId
	 * @param employeeById
	 * @param time
	 */
	public void addTimeToProject(Project project, UUID activityId, Employee employeeById, double time) {
		for (int i = 0; i < project.getActivities().size(); i++) {
			if (project.getActivities().get(i).getId().equals(activityId)) {
				HashMap<Employee,Double> work = project.getActivities().get(i).getWorkTime();
				if (work.containsKey(employeeById)) {
					work.put(employeeById, work.get(employeeById) + time);
				} else {
					work.put(employeeById, time);
				}
				break;
			}
		}
	}
	
	
	/**
	 * get activity by id
	 * @throws Exception 
	 */
	
	public Activity getActivityById(Project project, UUID id) throws Exception {
		for (int i = 0; i < project.getActivities().size(); i++) {
			if (project.getActivities().get(i).getId().equals(id)) {
				return project.getActivities().get(i);
			}
		}
		throw new Exception("Activity not found");
	}

	/**
	 * create a new activity for a given project and assign employees
	 * 
	 * @param project2
	 * @param startDate
	 * @param endDate
	 * @param activityName
	 * @param employees
	 */
	public void createActivity(Project project, LocalDate startDate, LocalDate endDate, String activityName, String budgetTime,
			List<Employee> employees) {
		Activity activity = new Activity(activityName,startDate,endDate);
		project.setActivities(activity);

		if (!budgetTime.equals("")) {
			activity.setBudgetTime(Double.parseDouble(budgetTime));
		}
		
		employees.forEach((e) -> {
			activity.setWorkTime(e,0.0);
		});
	}

	public void updateActivity(Project project, UUID activityId, LocalDate startDate, LocalDate endDate,
			String activityName, String budget, List<Employee> employees) throws Exception {

		Activity activity = this.getActivityById(project, activityId);
		
		activity.setActivityName(activityName);
		activity.setStartDate(startDate);
		activity.setEndDate(endDate);


		if (!budget.equals("")) {
			activity.setBudgetTime(Double.parseDouble(budget));
		}
		
		employees.forEach((e) -> {
			activity.setWorkTime(e,0.0);
		});
		
	}
	
//	
//	/**
//	 * activity
//	 */
//	
//	public Activity createActivity(Project project, String activityName) {
//		project.setActivities(new Activity(activityName));
//		return project.getActivities().get(project.getActivities().size() - 1);
//	}
}
