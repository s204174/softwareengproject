package domain;

import java.util.ArrayList;
import java.util.UUID;

 
public class Application {
	

	// in-memory database to search all projects and employees (implemented instead of db)
	public static ArrayList<Employee> employees = new ArrayList<Employee>();
	public static ArrayList<Project> projects = new ArrayList<Project>();

	/**
	 * login user based on credentials
	 * @param employeeId
	 * @param password
	 */
	public boolean login(String employeeId, String password) throws UserNotFoundException {
	
		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getEmployeeId().equals(employeeId) && employees.get(i).getPassword().equals(password)) {
				employees.get(i).setIsUserLoggedin(true);
				return true;
			}
		}
		throw new UserNotFoundException("Login invalid");
	}

	/**
	 * logout user based on employeeid
	 * @param employeeId
	 * @param password
	 */
	public void logout(String employeeId) {
	
		for (int i = 0; i < employees.size(); i++) {
			if (employees.get(i).getEmployeeId().equals(employeeId)) {
				employees.get(i).setIsUserLoggedin(false);
			}
		}
	}

}
