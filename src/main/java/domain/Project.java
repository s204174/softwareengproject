package domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Project {
	
	private ProjectManager projectManager; 
	private String projectName;
	
	private List<Activity> activites = new ArrayList<Activity>();
	private List<Employee> employeeList;
		
	private final String id;
	
	
	
	public Project() {
		
		// formatted from YYYY and random() to become fx: "211234"
		this.id = Integer.toString(Calendar.getInstance().get(Calendar.YEAR)).substring(2) 
					+ Integer.toString(Math.abs(new Random().nextInt())).substring(0,4); 
	}
	

	/**
	 * autogenerated report based on project for cooperate actions
	 * @return all project information
	 */
	public Project generateReport() {
		return this;
	}
	
	/**
	 * method to get all employees that have worked on the project
	 * instead of having to access project->activites->worktime->key every time
	 * 
	 * @return employees that have done work on the project
	 */
	
	public List<Employee> getEmployeesOnProject(){
		employeeList = new ArrayList<Employee>();
		
		this.getActivities().forEach(activity -> {
			activity.getWorkTime().forEach((employee, time) -> {
				employeeList.add(employee);
			});
		});
		
		return employeeList;
	}

	/**
	 * get total time spend on project
	 * @return time spend on all activities
	 */
	public double getTotalTimeSpendOnProject() {
		double totalTimeSpendOnProject = 0;
		for (Activity a : this.getActivities()) {
			totalTimeSpendOnProject += a.getTotalSpendTimeOnActivity();
		}
		return totalTimeSpendOnProject;
	} 

	/**
	 * get total budgetted time for project
	 * @return total budget time
	 */
	public double getTotalBudgetTimeOnProject() {
		double budgetTimeOnProject = 0;
		for (Activity a : this.getActivities()) {
			budgetTimeOnProject += a.getBudgetTime();
		}
		return budgetTimeOnProject;
	} 
	
	
	
	
	
	
	/**
	 * get unique project id based on current year and a random 4 digit value
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * get project name or project id if project name is not set
	 * @return
	 */
	public String getProjectName() {
		if (projectName.equals("")) {
			return this.getId();
		}
		return projectName;
	}

	/**
	 * set a project name
	 * @param projectName
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * add project manager
	 * @return projectManager
	 */
	public ProjectManager getProjectManager() {
		return projectManager;
	}

	/**
	 * set a project manager
	 * @param projectManager
	 */
	public void setProjectManager(ProjectManager projectManager) {
		this.projectManager = projectManager;
	}


	/**
	 * returns a list of all activities there are in this project
	 * @return activities
	 */
	public List<Activity> getActivities() {
		return activites;
	}

	/**
	 * set activities
	 * @param activites
	 */
	public void setActivities(Activity activity) {
		this.activites.add(activity);
	}
	
}

