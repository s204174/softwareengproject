package domain;

import java.time.LocalDate;

public class AbsenceTime {

	private LocalDate startDate;
	private LocalDate endDate;
	private String reason;

	public AbsenceTime(LocalDate startDate, LocalDate endDate, String reason) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.reason = reason;
	}

	public LocalDate getStartDate() {
		return startDate;
	}
	
	public LocalDate getEndDate() {
		return endDate;
	}
	public String getReason() {
		return reason;
	}

}
