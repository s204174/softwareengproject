package domain;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;



public class Activity {
	
	private String activityName;
	private HashMap<Employee,Double> workTime = new HashMap<Employee,Double>();
	private UUID id = UUID.randomUUID();
	
	private double budgetTime;
	private LocalDate startDate;
	private LocalDate endDate;
	
	
	public Activity(String activityName) {
		this.activityName = activityName;
	}
 
	
	public Activity(
			String activityName, 
			LocalDate startDate,
			LocalDate endDate
	) {
		this.activityName = activityName;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	 

	/**
	 * calculate total time spend by all employees on activity
	 * @return time spend on activity
	 */
	public double getTotalSpendTimeOnActivity() {
		double totalWorkTime = 0;
		for (Map.Entry<Employee, Double> work : this.getWorkTime().entrySet()) {
			totalWorkTime += work.getValue();
		}
		return totalWorkTime;
	}
	
	
	
	/**
	 * get id
	 */
	public UUID getId() {
		return this.id;
	}

	/**
	 * get the name of the activity
	 * @return activity description/name
	 */
	public String getActivityName() {
		return activityName;
	}

	/**
	 * set the name of the activity
	 * @param activityName
	 */
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
	/**
	 * get date of activity start date
	 * @return date
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * get date of activity end date by week number
	 * @return weeknumber
	 */
	public int getStartDateAsWeek() {
		if (startDate==null) {
			return 0;
		}
		WeekFields weeks = WeekFields.of(Locale.getDefault()); 
		return startDate.get(weeks.weekOfWeekBasedYear());
	}

	/**
	 * set date of activity start date
	 * @param date
	 */
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}


	/**
	 * get date of activity end date aka deadline
	 * @return date
	 */
	public LocalDate getEndDate() {
		return endDate;
	}
	
	
	/**
	 * get date of activity end date by week number
	 * @return weeknumber
	 */
	public int getEndDateAsWeek() {
		if (endDate==null) {
			return 0;
		}
		WeekFields weeks = WeekFields.of(Locale.getDefault()); 
		return endDate.get(weeks.weekOfWeekBasedYear());
	}

	/**
	 * set date of activity end date aka deadline
	 * @param date
	 */
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}


	/**
	 * returns the amount each employee has worked on the activity
	 * @return worktime by employee
	 */
	public HashMap<Employee, Double> getWorkTime() {
		return workTime;
	}

	/**
	 * sets the amount all employees have worked on the activity
	 * @param workTime
	 */
	public void setWorkTime(HashMap<Employee, Double> workTime) {
		this.workTime = workTime;
	}

	/**
	 * sets the amount of time one employee has worked on the activity
	 * @param employee object
	 * @param time spend
	 */
	public void setWorkTime(Employee e, Double t) {
		workTime.put(e, t);
	}

	 
	public double getBudgetTime() {
		return budgetTime;
	}


	public void setBudgetTime(double budgetTime) {
		this.budgetTime = budgetTime;
	}

}
