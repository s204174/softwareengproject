package domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class Employee {
	
	private boolean isUserLoggedin = false;
	
	private final String employeeId = Integer.toHexString(new Random().nextInt()).substring(0, 4);
	
	private String name;
	private String password;
	
	private List<AbsenceTime> absence = new ArrayList<AbsenceTime>();
	private List<Activity> activities = new ArrayList<Activity>();


	public Employee(String name) {
		this.name = name;
	}
	
	
	/**
	 * check if the user is a project manager
	 * @return false | true if employee is also project manager
	 */
	public boolean projectManagerExist() {
		if (this instanceof ProjectManager) {
			return true;
		}
		return false;
	}


	/**
	 * functionality of the application requires a user to be logged in
	 * attribute implemented in case of future usage where API is public
	 * 
	 * @return boolean
	 */
	public boolean getIsUserLoggedin() {
		return isUserLoggedin;
	}

	
	/**
	 * set user is logged in
	 * @param isUserLoggedin
	 */
	public void setIsUserLoggedin(boolean isUserLoggedin) {
		this.isUserLoggedin = isUserLoggedin;
	}

	
	/**
	 * get unique identifier of employee
	 * @return employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}
	
	/**
	 * get name of the employee
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * set name of the employee
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	

	/**
	 * get password
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * set an employees password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * get an employees activities
	 * @return list of activity user is assigned to
	 */
	public List<Activity> getActivities() {
		return activities;
	}
	
	
	/**
	 * 
	 * @return absence list
	 */
	public List<AbsenceTime> getAbsence() {
		return absence;
	}

	/**
	 * sets a start-, end date and reason for absence
	 * @param absence list
	 */
	public void setAbsence(List<AbsenceTime> absence) {
		this.absence = absence;
	}

	/**
	 * sets a start-, end date and reason for absence
	 * @param absence object
	 */
	public void setAbsence(AbsenceTime absence) {
		this.absence.add(absence);
	}

	
	/**
	 * set a new activity for the employee
	 * @param activities
	 */
	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	
	/**
	 * add a new activity to existing list
	 * @param activity
	 */
	public void setActivity(Activity activity) {
		activities.add(activity);
	}
	
}
