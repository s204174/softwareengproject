package domain;

import java.util.ArrayList;
import java.util.List;

public class ProjectManager extends Employee {
 	
	private List<Project> projects = new ArrayList<Project>();
	
	/**
	 * create manager by name if employee does not exist
	 * @param name
	 */
	public ProjectManager(String name) {
		super(name);
	}
	
	/**
	 * create manager by employee
	 * @param employee
	 */
	public ProjectManager(Employee employee) {
		super(employee.getName());
	}
	
	
	/**
	 * get project manager name
	 */
	public String getName() {
		return super.getName();
	}

	/**
	 * get project managers projects
	 * @return managers assigned projects
	 */
	public List<Project> getProjects() {
		return projects;
	}

	/**
	 * add projects to project manager
	 * @param project-object
	 */
	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	/**
	 * add a new project to project managers assigned project
	 * @param project-object
	 */
	public void setProjects(Project project) {
		this.projects.add(project);
	}
	

}
