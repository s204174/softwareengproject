package view;

import static view.Init.controller;

import java.time.LocalDate;

import domain.AbsenceTime;
import domain.Activity;
import domain.Employee;
import domain.Project;
import domain.ProjectManager;

public class DummyData {
	public DummyData() {
		createUsers();
		
	}
	
	public void createUsers() {

		Employee e = null;
		try {
			e = controller.createUser("1", "Lukas Mittun", "123321");
		e.setAbsence(new AbsenceTime(LocalDate.now(),LocalDate.parse("2021-09-21"),"Vacation"));
		} catch (Exception q) {
			q.printStackTrace();
		}
		
		// absence (45) - done
		// create account (60) - done
		// view my account (30) - done
		
		// 1.30

		// edit project (20) - done
		// activity add time (20) - done
		// view my activities on main page (30) - done
		
		// 5
		
		// edit activity (30) + view
		// activity new (45)
		
		// 6	 
		
		// view project report (60)
		
		

		Project p1 = controller.createProject("Skatteoptimering");
		p1.setActivities(new Activity("Oprette ApS"));
		controller.createProject("Skattely muligheder i panama");
		controller.createProject("Eksamens projekt");
		
		Project p = controller.createProject("Eksamens projekt 2");
		p.setActivities(new Activity("Udarbejdelse af use case diagram ud fra kundens beskrivelse"));
		p.getActivities().get(0).setWorkTime(e, 3.0);
//		p.getActivities().get(p.getActivities().size() - 1).setWorkTime(null);
		p.getActivities().get(p.getActivities().size() - 1).setBudgetTime(10);
		p.setProjectManager(new ProjectManager(e));
		
		
		p.setActivities(new Activity("Udarbejdelse af detailed use case descriptions med cucumber features"));
		p.setActivities(new Activity("Udarbejdelse af step definitions og domain layer"));
		p.setActivities(new Activity("Udarbejdelse af controller logik ud fra domain"));
		p.setActivities(new Activity("Udarbejdelse af grafisk brugergrænseflade til internt brug"));
		
	}
}
