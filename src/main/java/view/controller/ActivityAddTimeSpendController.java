package view.controller;

import static view.Init.controller;
import static view.Init.employeeId;
import static view.controller.ActivityController.activityId;
import static view.controller.ProjectNavigationController.project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import domain.Activity;
import domain.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

public class ActivityAddTimeSpendController {
	
	@FXML private TextField timeToAdd;
	@FXML private GridPane addHoursWrapper;
	
	
	@FXML
	private void initialize() throws Exception {
		int actY = 1;
		
		Activity a = controller.getActivityById(project, activityId);
		
		for (Entry<Employee, Double> e : a.getWorkTime().entrySet()) {
			HBox hours = hourSnippet(e.getKey(), e.getValue(), actY); // x position is set in activity snippet
			addHoursWrapper.getChildren().addAll(hours.getChildren());
			actY++;
		}
		 
	}
	
	private HBox hourSnippet(Employee e, Double hours, int actY) {

		HBox wrapper = new HBox();
		

		Label type = new Label(e.projectManagerExist()?"Manager":"Employee");
		GridPane.setRowIndex(type, actY);
		GridPane.setColumnIndex(type, 0);
		wrapper.getChildren().add(type);

		Label eId = new Label(e.getEmployeeId());
		GridPane.setRowIndex(eId, actY);
		GridPane.setColumnIndex(eId, 1);
		wrapper.getChildren().add(eId);

		Label name = new Label(e.getName()!=null?e.getName():"NaN");
		GridPane.setRowIndex(name, actY);
		GridPane.setColumnIndex(name, 2);
		wrapper.getChildren().add(name);

		Label Lhours = new Label(hours.toString());
		GridPane.setRowIndex(Lhours, actY);
		GridPane.setColumnIndex(Lhours, 3);
		wrapper.getChildren().add(Lhours);

		return wrapper;

	}
	
	@FXML
	private void addTime(ActionEvent event) throws Exception {
		if (!timeToAdd.getText().equals("")) {
			
			 try {
			   double time = Double.parseDouble(timeToAdd.getText()); 
			   controller.addTimeToProject(project, activityId, controller.getEmployeeById(employeeId),time);
				
				try {
					timeToAdd.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ActivityAddTimeSpendView.fxml")));
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }catch(NumberFormatException e) {
				Alert alert = new Alert(AlertType.INFORMATION, e.getMessage(), ButtonType.OK);
				alert.showAndWait();
			 } 
		}
	}
}
