package view.controller;

import static view.Init.controller;
import static view.Init.employeeId;

import java.io.IOException;
import java.time.LocalDate;
import java.util.UUID;

import domain.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class AbsenceManagementController {

	@FXML DatePicker fromDate;
	@FXML DatePicker toDate;
	@FXML TextArea reason;
	@FXML Button submitAbsence;
	
	@FXML GridPane myAbsenceList;
	
	@FXML
	public void initialize() {
		Employee e;
		try {
			
			String reason;
			LocalDate fromdate;
			LocalDate todate;
			int absY = 1;
			
			e = controller.getEmployeeById(employeeId);
			for (int i = e.getAbsence().size() - 1; i >= 0; i--) {
				
				reason = e.getAbsence().get(i).getReason();
				fromdate = e.getAbsence().get(i).getStartDate();
				todate = e.getAbsence().get(i).getEndDate();
				
				VBox snippet = AbsenceSnippet(employeeId,e.getName(),fromdate,todate,reason,absY);
				myAbsenceList.getChildren().addAll(snippet.getChildren());
				
				absY++;
						
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	}
	

	private VBox AbsenceSnippet(String employeeId, String name, LocalDate fromdate, LocalDate todate, String reason, int absY) {
		
		VBox wrapper = new VBox();


		Label eId = new Label(employeeId);
		GridPane.setRowIndex(eId, absY);
		GridPane.setColumnIndex(eId, 0);
		wrapper.getChildren().add(eId);


		Label eName = new Label(name);
		GridPane.setRowIndex(eName, absY);
		GridPane.setColumnIndex(eName, 1);
		wrapper.getChildren().add(eName);

		Label res = new Label(reason);
		GridPane.setRowIndex(res, absY);
		GridPane.setColumnIndex(res, 2);
		wrapper.getChildren().add(res);

		Label fDate = new Label(fromdate.toString());
		GridPane.setRowIndex(fDate, absY);
		GridPane.setColumnIndex(fDate, 3);
		wrapper.getChildren().add(fDate);
		
		Label tDate = new Label(todate.toString());
		GridPane.setRowIndex(tDate, absY);
		GridPane.setColumnIndex(tDate, 4);
		wrapper.getChildren().add(tDate);
		
		return wrapper;
	}
	
	@FXML
	private void createAbsence(ActionEvent event) {
		if (event.getSource().equals(submitAbsence) 
				&& !(fromDate.valueProperty().getValue() == null) 
				&& !(toDate.valueProperty().getValue() == null) 
				&& reason.getText() != "") {
			
			LocalDate from = fromDate.valueProperty().getValue();
			LocalDate to = toDate.valueProperty().getValue();
			String leaveReason = reason.getText();
			controller.setAbsence(employeeId,from,to,leaveReason);
		
			try {
				submitAbsence.getScene().setRoot(FXMLLoader.load(getClass().getResource("../AbsenceManagementView.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
}
