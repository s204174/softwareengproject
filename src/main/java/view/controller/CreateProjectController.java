package view.controller;
import static view.Init.controller;

import java.io.IOException;

 
import domain.Project;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
 
public class CreateProjectController {
 
		/**
		 * create project fields (projectNameField also used in update project)
		 */
		@FXML public TextField projectNameField;
		@FXML public Button createProjectBtn;
		
	 
		// store project info
		private Project project;
	 
		
		/**
		 * create project on submit on createproject page
		 * @throws Exception 
		 */
		@FXML
		private void createProject() throws Exception {
			String projectName = (!projectNameField.getText().equals("")) ? projectNameField.getText() : "" ;
			project = controller.createProject(projectName);
			if (project != null) {
				// sucessfully created project
				ProjectNavigationController.projectId = project.getId();
				
				projectNameField.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ProjectView.fxml")));
 				
			} else {

				Alert alert = new Alert(AlertType.INFORMATION, "Could not create project", ButtonType.OK);
				alert.showAndWait();
			}
		}
	 
	
}
