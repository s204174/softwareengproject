package view.controller;

import static view.Init.controller;
import static view.controller.ProjectNavigationController.project;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import domain.Activity;
import domain.Application;
import domain.Employee;
import domain.Project;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
 
public class HomeController {
	
	@FXML public Text welcomeSnippet;
	@FXML public GridPane projectsList;
	@FXML public GridPane latestActivities;
	
	@FXML public VBox activityWrapper;
	
	
	@FXML
 	public void initialize() {
		
		int projectsX = 0;
		int projectsY = 0;
		VBox snippet;
		
		
		// set welcome snippet 
		
		try {
			welcomeSnippet.setText("Welcome back, " + controller.getEmployeeById(view.Init.employeeId).getName() + "!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		
		// set projects

		for (int i = Application.projects.size() - 1; i >= 0; i--) {

			snippet = ProjectSnippet(Application.projects.get(i).getProjectName(),
					Application.projects.get(i).getId());

			// set column and row positions
			GridPane.setColumnIndex(snippet, projectsX);
			GridPane.setRowIndex(snippet, projectsY);
			
			projectsList.getChildren().add(snippet);
			
			projectsX++;

			if (projectsX % 3 == 0) {
				projectsX = 0;
				projectsY++;
			}
			
		}


		
		// set activities 
 			
			int actY = 1;
			for (int j = Application.projects.size() - 1; j >= 0; j--) {
				Project p = Application.projects.get(j);
				for (int i = p.getActivities().size() - 1; i >= 0; i--) {
					Activity a = p.getActivities().get(i);

					HBox activity = activitySnippet(p, a, actY); // x position is set in activity snippet
					latestActivities.getChildren().addAll(activity.getChildren());
						actY++;
					
				}
			}
		 
		
	}

	/***
	 * the "projects" element loaded at the main page right after "welcome"
	 * @param name
	 * @return vbox snippet for hompage
	 */

	public VBox ProjectSnippet(String name, String projectId) {
		
	     VBox container = new VBox();
	     container.setStyle("-fx-border-color: #555; -fx-border-radius: 3; -fx-padding:10px;");
	     
	     Label projectTitle = new Label(name);
	     projectTitle.setStyle(" -fx-font-size:15px; -fx-padding: 5px 0 20px 0; -fx-font-family:SansSerif;");
	     
	     Button viewButton = new Button("View");

	     viewButton.setId("project");
	     viewButton.setOnAction(e -> { navigateApplication(e); });
	     viewButton.setAccessibleText(projectId); // project "id"
			
			
	     viewButton.setStyle("-fx-background-color: #004bff; -fx-font-family:SansSerif; -fx-text-fill:#fff; -fx-border-radius: 5;");
	    		 
	     container.getChildren().add(projectTitle);
	     container.getChildren().add(viewButton);
	     return container;
	}

	
	public HBox activitySnippet(Project p, Activity activity, int actY) {
		

		String activityName = activity.getActivityName();
		double budget = activity.getBudgetTime();
		double spendTime = activity.getTotalSpendTimeOnActivity();
		

		String employees = ""; 
		
		for (Map.Entry<Employee, Double> e : activity.getWorkTime().entrySet()) {
			employees += e.getKey().getName().equals(null) ? e.getKey().getEmployeeId() : e.getKey().getName();
			employees += " ";
		}
		
		HBox wrapper = new HBox();
		

		Label activityLabel = new Label(activityName);
		GridPane.setRowIndex(activityLabel, actY);
		GridPane.setColumnIndex(activityLabel, 0);
		wrapper.getChildren().add(activityLabel);

		Label assignedTo = new Label(employees.equals("") ? "NaN" : employees);
		GridPane.setRowIndex(assignedTo, actY);
		GridPane.setColumnIndex(assignedTo, 1);
		wrapper.getChildren().add(assignedTo);

		Label assignedAndWorkedHours = new Label(budget + " / " + spendTime);
		GridPane.setRowIndex(assignedAndWorkedHours, actY);
		GridPane.setColumnIndex(assignedAndWorkedHours, 2);
		wrapper.getChildren().add(assignedAndWorkedHours);

		
		VBox change = new VBox();
		
		Button changeButton = new Button("Manage activity");
		changeButton.setId("changeActivity");
		changeButton.setAccessibleHelp(p.getId());
		changeButton.setAccessibleText(activity.getId().toString());
		changeButton.setOnAction(e -> activityActions(e));
		changeButton.setStyle("-fx-border-radius: 0; -fx-pref-width:200; -fx-background-color: transparent; -fx-font-weight:bold; -fx-text-fill: #004b96;");
		change.getChildren().add(changeButton);
		GridPane.setRowIndex(change, actY);
		GridPane.setColumnIndex(change, 3);
		wrapper.getChildren().add(change);

		
		VBox addTime = new VBox();
		
		Button addTimeButton = new Button("Add time");
		addTimeButton.setId("addTime");
		addTimeButton.setAccessibleHelp(p.getId());
		addTimeButton.setAccessibleText(activity.getId().toString());
		addTimeButton.setOnAction(e -> activityActions(e));
		addTimeButton.setStyle("-fx-border-radius: 0; -fx-pref-width:150; -fx-background-color: transparent; -fx-font-weight:bold; -fx-text-fill: #004b96;");
		addTime.getChildren().add(addTimeButton);
		
		GridPane.setRowIndex(addTime, actY);
		GridPane.setColumnIndex(addTime, 4);
		wrapper.getChildren().add(addTime);
		
		
     
		return wrapper;
	}
	

	private void activityActions(ActionEvent event) {
		 try {
			 
			 ActivityController.activityId = UUID.fromString(((Node) event.getSource()).getAccessibleText());
			 ProjectNavigationController.projectId = ((Node) event.getSource()).getAccessibleHelp();
			 
			 if ( ((Node) event.getSource()).getId() == "addTime") {
				 activityWrapper.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ActivityAddTimeSpendView.fxml")));
			 } else if ( ((Node) event.getSource()).getId() == "changeActivity") {
				 activityWrapper.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ActivityView.fxml")));
			 }
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	}
	
	
	@FXML
	public void navigateApplication(ActionEvent event) {
		try {
			 if ( ((Node) event.getSource()).getId() == "project") {
				
				// show project 
				ProjectNavigationController.projectId =  ((Node) event.getSource()).getAccessibleText();
				welcomeSnippet.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/projectView.fxml")));
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
