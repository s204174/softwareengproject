package view.controller;

import java.io.IOException;
import java.util.UUID;

import domain.UserNotFoundException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import static view.Init.controller;
 
public class LoginController {
	
	@FXML public Button submit;
	@FXML public TextField employeeIdInput;
	@FXML public PasswordField pswInput;
	
	@FXML public Text dummyLogin;
	public static String dummyLoginString;
	
	public void initialize() {
		if (dummyLogin!=null) {
			
			dummyLogin.setText(dummyLoginString);
		}
	}
	
	/**
	 * Sign in, validate and route a registered account
	 * 
	 * @param event
	 * @throws IOException
	 * @throws UserNotFoundException 
	 */
	@FXML 
	private void handleButtonClick(ActionEvent event) throws IOException {

		if (event.getSource() == submit) {
			try {
				controller.login(employeeIdInput.getText(), pswInput.getText());
				view.Init.employeeId = employeeIdInput.getText();
				submit.getScene().setRoot(FXMLLoader.load(getClass().getResource("../HomeView.fxml")));

			} catch(UserNotFoundException e) {
				Alert alert = new Alert(AlertType.INFORMATION, e.getMessage(), ButtonType.OK);
				alert.showAndWait();
			}
		}
		
	}

}
