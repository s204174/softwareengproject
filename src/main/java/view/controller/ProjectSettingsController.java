package view.controller;
import static view.Init.controller;
import static view.controller.ProjectNavigationController.project;

import java.io.IOException;

import domain.Employee;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class ProjectSettingsController {
 
 
		@FXML public TextField projectNameField; 
		@FXML public TextField projectManagerField;
		@FXML public Button updateProjectButton;
		 
		@FXML 
		public void initialize() {
 				projectNameField.setText(project.getProjectName());
				projectManagerField.setText((project.getProjectManager()==null)?"":project.getProjectManager().getEmployeeId());
 		}

		
		@FXML
		private void updateProject() {

			// update name
			
			if (!projectNameField.getText().equals("")) {
				try {
					controller.updateProjectName(project,projectNameField.getText());
				} catch (Exception e) {
					e.printStackTrace();
				} 
			}

			// update manager
			

			Employee manager;
			
			try {
				
				manager = controller.getEmployeeById(projectManagerField.getText());
				controller.updateProjectManager(project,manager);
				 
			} catch (Exception e1) {

				Alert alert = new Alert(AlertType.INFORMATION, e1.getMessage(), ButtonType.OK);
				alert.showAndWait();
			}
			
			
			try {
				projectNameField.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ProjectView.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// back button on project settings
		@FXML
		private void back() {
			try {
				projectNameField.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ProjectView.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	
}
