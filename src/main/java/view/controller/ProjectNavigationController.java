package view.controller;

import static view.Init.controller;

import java.io.IOException;
import java.net.URL;

import domain.Project;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

public class ProjectNavigationController {
	
	// store current project being used
	public static Project project = null;
	public static String projectId;
	 
	/**
	 * view proiject fields
	 */
	@FXML private Text projectNameLabel;
	@FXML private Text projectManagerOfProject;
	
	
	/**
	 * navigation
	 * all onAction set to projectNavigation
	 */
	@FXML private Button settings;
	@FXML private Button projectActivities;
	@FXML private Button newActivity;
	@FXML private Button projectReport;
	public static String page = "";
	 
    
	@FXML
	private void initialize() throws Exception {

		// get project if project or project id is null or a new project is loaded (and project id's dont match)
		if (project == null && projectId != null || (projectId != project.getId())) {
			project = controller.getProjectById(projectId);
		}
		
		
		projectNameLabel.setText(project.getProjectName());
		projectManagerOfProject.setText(project.getProjectManager()!=null?"Managed by "+project.getProjectManager().getName():"No project manager assigned");

		if (page.equals("../project/ReportView.fxml")) {
			// toggle between button color to show which page is active
			projectActivities.setTextFill(Paint.valueOf("#004b96"));
			projectReport.setTextFill(Paint.valueOf("#fff"));
			projectReport.setStyle("-fx-background-color: #004b96; -fx-border-color: #004b96; -fx-border-width: 1 2 1 2;");
			projectActivities.setStyle("-fx-background-color: transparent; -fx-border-color: #004b96; -fx-border-width: 2 2 2 2;");
 			 
		}
		
	}
	
	@FXML 
	private void projectNavigation(ActionEvent event) {

		try {
			Object e = event.getSource();
			 page = "ProjectView.fxml";
			
			if (e.equals(settings)) {
				page = "ProjectSettingsView.fxml";
	 			 
			} else if (e.equals(projectActivities)) {
				page = "ProjectView.fxml";
	 			 
			} else if (e.equals(newActivity)) {
				page = "ActivityView.fxml";
				view.controller.ActivityController.activityId = null;
	 			 
			} else if (e.equals(projectReport)) {
				page = "ReportView.fxml"; 
				
			}
 
			page = "../project/"+page;
			
			projectNameLabel.getScene().setRoot(FXMLLoader.load(getClass().getResource(page)));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
		
}
