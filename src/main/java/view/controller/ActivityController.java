package view.controller;

import static view.Init.controller;
import static view.controller.ProjectNavigationController.project;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import domain.Activity;
import domain.Application;
import domain.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ActivityController {
	
	public static UUID activityId;

	@FXML private DatePicker datePickerStart;
	@FXML private DatePicker datePickerEnd;
	@FXML private TextField activityDesc;
	@FXML private TextField employeesOnTask;
	@FXML private TextField budgetTime;

	@FXML private GridPane contributingEmployees;
	@FXML private GridPane availEmployees;
	@FXML private GridPane absenceList;
	
	// used for creating and updating activity	
	@FXML
	private void initialize() {
		if (activityId!=null) {
			try {
				Activity act = controller.getActivityById(project, activityId);

				datePickerStart.setValue(act.getStartDate());
				datePickerEnd.setValue(act.getEndDate());
				activityDesc.setText(act.getActivityName());
				budgetTime.setText(Double.toString(act.getBudgetTime()));
				
				String listOfEmployees = "";
				int currSize = 0;
				for (Map.Entry<Employee,Double> e : act.getWorkTime().entrySet()) {
					listOfEmployees += e.getKey().getEmployeeId();
					currSize++;
					if (act.getWorkTime().size() != currSize) { 
						listOfEmployees += ",";	
					}
				}
				employeesOnTask.setText(listOfEmployees);
				
				
				int absY = 1;
				// contributing employees
				
				for (Entry<Employee, Double> e : act.getWorkTime().entrySet()) {
					HBox hours = hourSnippet(e.getKey(), e.getValue(), absY); // x position is set in activity snippet
					contributingEmployees.getChildren().addAll(hours.getChildren());
					absY++;
				}
				
				// all employees
				absY = 1;
				for (int i = Application.employees.size() - 1; i >= 0; i--) {
					Employee e = Application.employees.get(i);
					
					VBox employee = accountSnippet(e, absY); // x position is set in activity snippet
					availEmployees.getChildren().addAll(employee.getChildren());
					absY++;
				} 
				
				
				
				// absence list 
				absY = 1;
				Employee e;
				for (int k = 0; k < Application.employees.size(); k++) {

					String reason;
					LocalDate fromdate;
					LocalDate todate;
					
					e = Application.employees.get(k);
					for (int i = e.getAbsence().size() - 1; i >= 0; i--) {
						
						reason = e.getAbsence().get(i).getReason();
						fromdate = e.getAbsence().get(i).getStartDate();
						todate = e.getAbsence().get(i).getEndDate();
						
						VBox snippet = AbsenceSnippet(e.getEmployeeId(),e.getName(),fromdate,todate,reason,absY);
						absenceList.getChildren().addAll(snippet.getChildren());
						
						absY++;
								
					}
				}
				
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@FXML
	private void activitySubmit(ActionEvent event) {
		if ( 
				!(activityDesc.getText().equals(""))
			) {
			
				List<Employee> employees = new ArrayList<Employee>();
				
				if (!employeesOnTask.getText().equals("")) {
					String[] eList = employeesOnTask.getText().split(",");
					try {

						if (eList.length == 0) {
							if (controller.getEmployeeById(employeesOnTask.getText()) instanceof Employee) {
								employees.add(controller.getEmployeeById(employeesOnTask.getText().trim()));
							}
						} else {
							for (int i = 0; i < eList.length; i++) {
								if (!eList[i].trim().equals("")) {
	
									Employee es = controller.getEmployeeById(eList[i].trim());
									if (es instanceof Employee) {
										employees.add(es);
									} 
									
								}
								
							}
						}
						
					} catch (Exception e) {
						Alert alert = new Alert(AlertType.INFORMATION, e.getMessage(), ButtonType.OK);
						alert.showAndWait();
					}
				}

				LocalDate startDate = datePickerStart.valueProperty().getValue();
				LocalDate endDate = datePickerEnd.valueProperty().getValue();
				String activityName = activityDesc.getText();
				String budget = budgetTime.getText();
				
				if (activityId == null) {
					controller.createActivity(project, startDate, endDate, activityName, budget, employees);
				} else {
					try {
						controller.updateActivity(project, activityId, startDate, endDate, activityName, budget, employees);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				try {
					datePickerStart.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ProjectView.fxml")));
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				 
			
			
		}
	}
	
	
	
	
	
	
	
	
	

	private VBox AbsenceSnippet(String employeeId, String name, LocalDate fromdate, LocalDate todate, String reason, int absY) {
		
		VBox wrapper = new VBox();


		Label eId = new Label(employeeId);
		GridPane.setRowIndex(eId, absY);
		GridPane.setColumnIndex(eId, 0);
		wrapper.getChildren().add(eId);


		Label eName = new Label(name);
		GridPane.setRowIndex(eName, absY);
		GridPane.setColumnIndex(eName, 1);
		wrapper.getChildren().add(eName);

		Label res = new Label(reason);
		GridPane.setRowIndex(res, absY);
		GridPane.setColumnIndex(res, 2);
		wrapper.getChildren().add(res);

		Label fDate = new Label(fromdate.toString());
		GridPane.setRowIndex(fDate, absY);
		GridPane.setColumnIndex(fDate, 3);
		wrapper.getChildren().add(fDate);
		
		Label tDate = new Label(todate.toString());
		GridPane.setRowIndex(tDate, absY);
		GridPane.setColumnIndex(tDate, 4);
		wrapper.getChildren().add(tDate);
		
		return wrapper;
	}
	

	private HBox hourSnippet(Employee e, Double hours, int actY) {

		HBox wrapper = new HBox();
		

		Label type = new Label(e.projectManagerExist()?"Manager":"Employee");
		GridPane.setRowIndex(type, actY);
		GridPane.setColumnIndex(type, 0);
		wrapper.getChildren().add(type);

		Label eId = new Label(e.getEmployeeId());
		GridPane.setRowIndex(eId, actY);
		GridPane.setColumnIndex(eId, 1);
		wrapper.getChildren().add(eId);

		Label name = new Label(e.getName()!=null?e.getName():"NaN");
		GridPane.setRowIndex(name, actY);
		GridPane.setColumnIndex(name, 2);
		wrapper.getChildren().add(name);

		Label Lhours = new Label(hours.toString());
		GridPane.setRowIndex(Lhours, actY);
		GridPane.setColumnIndex(Lhours, 3);
		wrapper.getChildren().add(Lhours);

		return wrapper;

	}
	

	private VBox accountSnippet(Employee e, int actY) {
		VBox wrapper = new VBox();

		Label eId = new Label(e.getEmployeeId());
		GridPane.setRowIndex( eId, actY);
		GridPane.setColumnIndex( eId, 0);
		wrapper.getChildren().add( eId);

		Label eName = new Label(e.getName().equals(null) ? "NaN" : e.getName());
		GridPane.setRowIndex( eName, actY);
		GridPane.setColumnIndex( eName, 1);
		wrapper.getChildren().add( eName);
		
		// checks if employee object is instance of projectmanager class
		Label eStatus = new Label(e.projectManagerExist() ? "Manager" : "Employee");
		GridPane.setRowIndex( eStatus, actY);
		GridPane.setColumnIndex( eStatus, 2);
		wrapper.getChildren().add( eStatus);

		return wrapper;
	}
	
}
