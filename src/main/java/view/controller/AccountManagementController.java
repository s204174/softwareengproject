package view.controller;


import static view.Init.controller;
import static view.Init.employeeId;
import static view.controller.ProjectNavigationController.project;

import java.io.IOException;

import domain.Application;
import domain.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class AccountManagementController {

	/**
	 * create account view
	 */
	@FXML private Button viewAccount;
	@FXML private GridPane accountsList;

	@FXML private TextField employeeTypeField;
	@FXML private TextField employeeNameField;
	@FXML private PasswordField passwordField;
	
	
	/**
	 * myaccountinformationview
	 */
	@FXML private Button back;
	@FXML private TextField myAccountType;
	@FXML private TextField myEmployeeId;
	@FXML private TextField myName;
	@FXML private TextField myPassword;
	
	String currentPage = ""; // set in showAccount
	
	@FXML
	private void initialize() {
		if (accountsList != null) { // add account view
			int actY = 1;
			for (int i = Application.employees.size() - 1; i >= 0; i--) {
				Employee e = Application.employees.get(i);
				
				VBox employee = accountSnippet(e, actY); // x position is set in activity snippet
				accountsList.getChildren().addAll(employee.getChildren());
				actY++;
			}
		} else { // my account view
			try {
				Employee me = controller.getEmployeeById(employeeId);
				myAccountType.setText(me.projectManagerExist() ? "Manager" : "Employee");
				myEmployeeId.setText(me.getEmployeeId());
				myName.setText(me.getName().equals(null) ? "NaN" : me.getName());
				myPassword.setText(me.getPassword());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			

	}
	
	private VBox accountSnippet(Employee e, int actY) {
		VBox wrapper = new VBox();

		Label eId = new Label(e.getEmployeeId());
		GridPane.setRowIndex( eId, actY);
		GridPane.setColumnIndex( eId, 0);
		wrapper.getChildren().add( eId);

		Label eName = new Label(e.getName().equals(null) ? "NaN" : e.getName());
		GridPane.setRowIndex( eName, actY);
		GridPane.setColumnIndex( eName, 1);
		wrapper.getChildren().add( eName);
		
		// checks if employee object is instance of projectmanager class
		Label eStatus = new Label(e.projectManagerExist() ? "Manager" : "Employee");
		GridPane.setRowIndex( eStatus, actY);
		GridPane.setColumnIndex( eStatus, 2);
		wrapper.getChildren().add( eStatus);

		return wrapper;
	}
	
	@FXML
 	public void showAccount(ActionEvent event) {
		if (event.getSource().equals(viewAccount)) {
			try {
				viewAccount.getScene().setRoot(FXMLLoader.load(getClass().getResource("../MyAccountInformationView.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (event.getSource().equals(back)) {

			try {
				back.getScene().setRoot(FXMLLoader.load(getClass().getResource("../AccountManagementView.fxml")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	@FXML 
	public void createAccount(ActionEvent event) {
		if (!(employeeTypeField.getText().equals("")) 
				&& !(employeeNameField.getText().equals("")) 
				&& !(passwordField.getText().equals(""))){
			String eType = employeeTypeField.getText().trim();

			if (eType.equals("1") || eType.equals("2")) {
				String eName = employeeNameField.getText().trim();
				String ePsw  = passwordField.getText().trim();
				try {
					controller.createUser(eType, eName, ePsw);
					employeeTypeField.getScene().setRoot(FXMLLoader.load(getClass().getResource("../AccountManagementView.fxml")));
				} catch (Exception e) {
					Alert alert = new Alert(AlertType.INFORMATION, e.getMessage(), ButtonType.OK);
					alert.showAndWait();
				};
			} else {
				Alert alert = new Alert(AlertType.INFORMATION, "Invalid account type entered", ButtonType.OK);
				alert.showAndWait();
			}
		}
	}
}
