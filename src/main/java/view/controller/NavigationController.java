package view.controller;


import static view.Init.controller;
import static view.Init.employeeId;

import java.io.IOException;

import domain.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.VBox;

public class NavigationController {
	
	@FXML private Hyperlink home;
	@FXML private Hyperlink accountManagement;
	@FXML private Hyperlink absenceManagement;
	@FXML private Hyperlink logOut;
	@FXML private Button newProject;
	
	@FXML private VBox projectsOverview;
	
	@FXML
	public void initialize() {
		
		for (int i = Application.projects.size() - 1; i >= 0; i--) {
			projectsOverview.getChildren().add(
					projectLink(Application.projects.get(i).getProjectName(),Application.projects.get(i).getId()));
		}

	}
	
 	public Hyperlink projectLink(String projectName, String projectId) {
		Hyperlink h = new Hyperlink(projectName != null ? projectName : projectId);
		h.setStyle("-fx-underline: false; -fx-text-color:#ffffff; -fx-font-size:14px; -fx-border-color: transparent;");
		h.setId("project");
		h.setOnAction(e -> { navigateApplication(e); });
		h.setAccessibleText(projectId); // project "id"
		return h;
	}
 	 
	
	@FXML
	public void navigateApplication(ActionEvent event) {
 		try {
			if (event.getSource() == home) {
				projectsOverview.getScene().setRoot(FXMLLoader.load(getClass().getResource("../HomeView.fxml")));
			
			} else if (event.getSource() == accountManagement) {
				projectsOverview.getScene().setRoot(FXMLLoader.load(getClass().getResource("../AccountManagementView.fxml")));
			
			} else if (event.getSource() == absenceManagement) {
				projectsOverview.getScene().setRoot(FXMLLoader.load(getClass().getResource("../AbsenceManagementView.fxml")));
			
			} else if (event.getSource() == logOut) {
				// log out user api
				controller.logout(employeeId);
				projectsOverview.getScene().setRoot(FXMLLoader.load(getClass().getResource("../LoginView.fxml")));
		
			} else if ( ((Node) event.getSource()).getId() == "project") {
				
				// show project 
				ProjectNavigationController.projectId =  ((Node) event.getSource()).getAccessibleText();
				projectsOverview.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/projectView.fxml")));
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * create new project
	 * @param event
	 * @throws IOException
	 */
	@FXML
	public void navigateProject(ActionEvent event) {
		try {
			if (event.getSource() == newProject) {
				newProject.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/CreateProjectView.fxml")));
			}
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
