package view.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import static view.controller.ProjectNavigationController.project;

import java.util.Map.Entry;

import domain.Employee;

public class ProjectReportController {
	
	
	@FXML VBox pieWrapper;

	@FXML Text time;

	@FXML Text budgettime;
	
	@FXML private VBox activities;
	
	public void initialize() {
		
		
//		
//		pieWrapper.setStyle("-fx-pref-height:500; -fx-pref-width:500;");
//
//		  ObservableList<PieChart.Data> pieChartData =
//	                FXCollections.observableArrayList(
//	                new PieChart.Data("Grapefruit", 13),
//	                new PieChart.Data("Oranges", 25),
//	                new PieChart.Data("Plums", 10),
//	                new PieChart.Data("Pears", 22),
//	                new PieChart.Data("Apples", 30));
//	        final PieChart chart = new PieChart(pieChartData);
//	        chart.setTitle("Imported Fruits");
//
//	        pieWrapper.getChildren().add(chart);
		
		

		budgettime.setText("Total time budgetted on project: " + project.getTotalBudgetTimeOnProject() + "hours");
		time.setText("Total time spend on project: " + project.getTotalBudgetTimeOnProject() + "hours");
//
//		int absY = 1;
//		// contributing employees
//		for (int i = 0; i < project.getActivities().size(); i++) {
//
//			if (project.getActivities().get(i).getWorkTime().size() != 0) {
//				for (Entry<Employee, Double> e : project.getActivities().get(i).getWorkTime().entrySet()) {
//					HBox hours = hourSnippet(e.getKey(), e.getValue(), absY); // x position is set in activity snippet
//					activities.getChildren().addAll(hours.getChildren());
//					absY++;
//				}
//			}
//		}
//		
	}

	private HBox hourSnippet(Employee e, Double hours, int actY) {

		HBox wrapper = new HBox();
		

		Label type = new Label(e.projectManagerExist()?"Manager":"Employee");
		GridPane.setRowIndex(type, actY);
		GridPane.setColumnIndex(type, 0);
		wrapper.getChildren().add(type);

		Label eId = new Label(e.getEmployeeId());
		GridPane.setRowIndex(eId, actY);
		GridPane.setColumnIndex(eId, 1);
		wrapper.getChildren().add(eId);

		Label name = new Label(e.getName()!=null?e.getName():"NaN");
		GridPane.setRowIndex(name, actY);
		GridPane.setColumnIndex(name, 2);
		wrapper.getChildren().add(name);

		Label Lhours = new Label(hours.toString());
		GridPane.setRowIndex(Lhours, actY);
		GridPane.setColumnIndex(Lhours, 3);
		wrapper.getChildren().add(Lhours);

		return wrapper;

	}
	

}
