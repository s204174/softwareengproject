package view.controller;

import static view.controller.ProjectNavigationController.project;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import domain.Activity;
import domain.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

 
public class ProjectController {

	@FXML private GridPane allActivities;
	@FXML private VBox activityWrapper;
	
	public void initialize() {
		if (!project.getId().equals("")) { // make sure project id exist
			if (project.getActivities().size() > 0) {
				
 				int actY = 1;
				for (int i = project.getActivities().size() - 1; i >= 0; i--) {
					Activity a = project.getActivities().get(i);

					HBox activity = activitySnippet(a, actY); // x position is set in activity snippet
 					allActivities.getChildren().addAll(activity.getChildren());
 					actY++;
					
				}
			} else {
				// project has no activities
				
				Label label = new Label("Project has no activities.");
				label.setStyle("-fx-pref-width:900; -fx-font-size:18px; -fx-text-alignment:center;");
			
				label.setPrefWidth(900.0);
				label.setAlignment(Pos.CENTER);
				activityWrapper.getChildren().add(label);
				
			}
		}
		
		
	}
	

	
	public HBox activitySnippet(Activity activity, int actY) {
		

		String activityName = activity.getActivityName();
		double budget = activity.getBudgetTime();
		double spendTime = activity.getTotalSpendTimeOnActivity();
		
		
		//somedate.get(WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear());

		String employees = ""; 
		
		for (Map.Entry<Employee, Double> e : activity.getWorkTime().entrySet()) {
			employees += e.getKey().getName().equals(null) ? e.getKey().getEmployeeId() : e.getKey().getName();
			employees += " ";
		}
		
		HBox wrapper = new HBox();
		

		Label activityLabel = new Label(activityName);
		GridPane.setRowIndex(activityLabel, actY);
		GridPane.setColumnIndex(activityLabel, 0);
		wrapper.getChildren().add(activityLabel);

		Label startend = new Label("w"+Integer.toString(activity.getStartDateAsWeek())+"/w"+Integer.toString(activity.getEndDateAsWeek()));
		GridPane.setRowIndex(startend, actY);
		GridPane.setColumnIndex(startend, 1);
		wrapper.getChildren().add(startend);
		
		Label assignedTo = new Label(employees.equals("") ? "NaN" : employees);
		GridPane.setRowIndex(assignedTo, actY);
		GridPane.setColumnIndex(assignedTo, 2);
		wrapper.getChildren().add(assignedTo);

		Label assignedAndWorkedHours = new Label(budget + " / " + spendTime);
		GridPane.setRowIndex(assignedAndWorkedHours, actY);
		GridPane.setColumnIndex(assignedAndWorkedHours, 3);
		wrapper.getChildren().add(assignedAndWorkedHours);

		
		VBox change = new VBox();
		
		Button changeButton = new Button("Manage activity");
		changeButton.setId("changeActivity");
		changeButton.setAccessibleText(activity.getId().toString());
		changeButton.setOnAction(e -> activityActions(e));
		changeButton.setStyle("-fx-border-radius: 0; -fx-pref-width:150; -fx-background-color: transparent; -fx-font-weight:bold; -fx-text-fill: #004b96;");
		change.getChildren().add(changeButton);
		GridPane.setRowIndex(change, actY);
		GridPane.setColumnIndex(change, 4);
		wrapper.getChildren().add(change);

		
		VBox addTime = new VBox();
		
		Button addTimeButton = new Button("Add time");
		addTimeButton.setId("addTime");
		addTimeButton.setAccessibleText(activity.getId().toString());
		addTimeButton.setOnAction(e -> activityActions(e));
		addTimeButton.setStyle("-fx-border-radius: 0; -fx-pref-width:100; -fx-background-color: transparent; -fx-font-weight:bold; -fx-text-fill: #004b96;");
		addTime.getChildren().add(addTimeButton);
		
		GridPane.setRowIndex(addTime, actY);
		GridPane.setColumnIndex(addTime, 5);
		wrapper.getChildren().add(addTime);
		
		
     
		return wrapper;
	}
	

	private void activityActions(ActionEvent event) {
		 try {
			 
			 ActivityController.activityId = UUID.fromString(((Node) event.getSource()).getAccessibleText());
			 
			 if ( ((Node) event.getSource()).getId() == "addTime") {
				 activityWrapper.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ActivityAddTimeSpendView.fxml")));
			 } else if ( ((Node) event.getSource()).getId() == "changeActivity") {
				 activityWrapper.getScene().setRoot(FXMLLoader.load(getClass().getResource("../project/ActivityView.fxml")));
			 }
		 } catch (IOException e) {
			 e.printStackTrace();
		 }
	}
	
	
	
	
	
}
