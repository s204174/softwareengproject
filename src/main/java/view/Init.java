
package view;


import controller.FrontController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Init extends Application { 

	public static FrontController controller = FrontController.getInstance();
	public static String employeeId;
 
    @Override 
    public void start(Stage primaryStage) throws Exception {
        try {
            
            FXMLLoader loader = new FXMLLoader();
            primaryStage.setTitle("Softwarehuset A/S | Project management");
            
            loader.setLocation(
                    Init.class.getResource(
                            "./LoginView.fxml"
                    )
            );
            Parent root = loader.load();
       
            Scene scene = new Scene(root, 1280, 800);
           
            boolean resizable = primaryStage.isResizable();
            primaryStage.setResizable(!resizable);

            primaryStage.setScene(scene);
            primaryStage.show();
                        
        } catch (Exception e){
            System.out.println(e);
        }
           
         
    }


    public static void main(String[] args) {
    	new DummyData(); // creates some dummy data so we can show the application on exam day
        launch(args);
    }

    
    
}