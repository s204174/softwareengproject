Feature: Registrere timer til aktivitet
  Description:
     Employee can register time spend on a activity.

  Actors:
    Employee.
  
Background:
  Given User is registered under softwarehus A/S
  And there exists a project 
  And the project has an activity
  
Scenario: employee adds actual spend time on activity
  Given the employee is assigned to the activity.
  When the employee has "3" hours of time on activity
  Then "3"  hours are registered for the activity
   
Scenario: multiple employees add actual time spend
  Given the employee is assigned to the activity.
  And the employee has "3" hours of time on activity
  And another users then adds "5" hours of time worked on the activity
  Then the time both employees have worked is registered on the activity