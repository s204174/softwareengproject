Feature: Tilfoej projekt
  Description: 
    Add or make a new project. 
    
  Actors:
    Project manager, customer & Softwarehus A/S.

Background:
  Given User is registered under softwarehus A/S
  
Scenario: Create new Project
  When a user creates a new project called "x" in year twenty-twenty-one
  Then a new project is created with an id number (year_postfix-unique_id)
  
  
Scenario: Assign project manager to project
  Given there exists a project
  And there is a project with title "x"
  And project "x" has no project manager
  When project manager is assigned to project
  Then project "x" has user as manager


 Scenario: A project manager is assigned to a project that already has a project manager
    Given there exists a project
    And the project has a project manager
    Then an employee is assigned as the new project manager

# Optional med navnet på projektet.
Scenario: Project manager changes the name of a project
  	Given there exists a project
	And the project has a project manager
  	And there is a project with title "x"
    When the user changes the name to "y"
    Then the name of the project is "y"

    
