Feature: Log in
  Description:
	users should be able to log in to their account to manage their activities and projects
  
  Actors:
	anyone with an account
    
Scenario: A valid account tries to log in
    Given an account with the name "Lukas Mittun" and password "123123" exists
    And the user logs in with the employee id and password "123123"
    Then the user is loggedin
    
Scenario: An account tries to log in with wrong password
    Given an account with the name "Lukas Mittun" and password "123123" exists
    And the user logs in with the employee id and password "1123"
    Then the error message "Login invalid" is given
    
Scenario: An account tries to log in with wrong employee id
    Given an account with the name "Lukas Mittun" and password "123123" exists
    And the user logs in with employee id "42e1b03b-7ec3-4dac-a9c3-52cdb196b97c" and password "123123"
    Then the error message "Login invalid" is given
   
 Scenario: Account log out
    Given an account with the name "Lukas Mittun" and password "123123" exists
   	And the user is logged in
   	And the user tries to log out
    Then the user is logged out