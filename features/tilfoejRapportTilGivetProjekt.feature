Feature: Tilfoej rapport til givet projekt
  Description: 
    Adds report for a given project.
    
  Actors:
    Project manager.
 
 Background:
     Given there exists a project
   	 And there is a project with title "test project 2"
   	 And the project has 2 activites
   	 And both activities have 10.00 hours as the budgetted time
   	 And activity 1 has one employee assigned that has worked 5.5 hours
   	 And activity 2 has 2 employees assigned that has worked 6.0 hours each
   	 And one of the employees have had absence between "2021-06-05" to "2021-06-10" with the reason "ferie"
   	
 
 
  Scenario: project manager requests a report
    Given the report contains all the projects activities
    And employees time and name that have spend time on each activity
    And absence of employees that have worked on the project
    And total spent time on each activity
    And total spent time on the project
    And budgetted time
    Then a report is returned to the user
