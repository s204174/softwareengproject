Feature: Registrere fravaer til aktivitet
  Description:
    Employee can register abscence spend on a activity.
    
  Actors:
    Employee.
 
Background:
  Given User is registered under softwarehus A/S
  
  
Scenario: The user registers abscence
  Given a user creates a new absence activity with a start-, end-date and reason for absence
  Then the absence is registered for the user
