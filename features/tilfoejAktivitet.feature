Feature: Tilfoej aktivitet
  Description:
    Adds activity.

  Actors: 
    Employee and costomer.

Background:
	Given there exists a project
    And there is a project with title "x"
    And the project has a project manager
	
Scenario: Project manager adds an activity to a project
	When the project manager adds an activity with start time "2021-05-10" and deadline "2021-05-18"
	Then the activity is added to the project

Scenario: Project manager changes the amount of budgeted time
   Given there exists an activity
	When the project manager changes the budgeted time of the activity to 100.00 hours
	Then the budgeted time of the activity is 100.00 hours
 