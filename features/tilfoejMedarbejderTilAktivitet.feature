Feature: Tilfoeje medarbejder til aktivitet
  Description:
    Adds Employee to activity.

  Actors: 
    Projekt manager.
    
Background:
    Given there exists a project
    And there is a project with title "test project"
    And the project has a project manager
    
Scenario: Assign employee to activity successfully
    Given the project has an activity
    And an account with the name "Lukas Mittun" and password "123123" exists
	And the employee is assigned to the activity.
    Then The employee is assigned the activity.
    