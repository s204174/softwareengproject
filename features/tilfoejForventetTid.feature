Feature: Tilfoej forventet tid
    Description:
        For each activity the project manager must state the expected number of workhours spend on the activity. 
        
    Actors:
     Project manager.


Scenario: project manager adds estimated time
  Given there exists a project
  And the project has a project manager
  And there exists an activity
  When the project manager changes the estimated time of the activity to 100.00 hours
  Then the estimated time of the activity is 100.00 hours
  
Scenario: Project manager changes the amount of budgeted time
  Given there exists a project
  And the project has a project manager
  And there exists an activity
  When the project manager changes the budgeted time of the activity to 100.00 hours
  Then the budgeted time of the activity is 100.00 hours
